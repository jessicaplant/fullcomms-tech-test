<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewSubmissionOwner;
use App\Mail\NewSubmissionUser;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function index() {
    	return view('messages.index', [ 'messages' => Message::all()]);
    }

    public function create() {
		return view('messages.create');
    }

    public function store(Request $request) {
		$request->validate([
			'name' => 'required',
			'email' => 'required|email',
			'subject' => 'required',
			'message' => 'required'
		]);
		$message = new Message([
			'name' => $request->get('name'),
			'email' => $request->get('email'),
			'subject' => $request->get('subject'),
			'message' => $request->get('message'),
		]);
		$message->save();
	    Mail::to('jessica@jessica-plant.co.uk')->send(new NewSubmissionOwner($message));
	    Mail::to($message->email)->send(new NewSubmissionUser());
		return redirect('/messages')->with('success', 'Your message has been successfully saved');
    }
}
