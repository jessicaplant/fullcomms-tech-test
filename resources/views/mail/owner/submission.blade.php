<table class="table">
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Subject</th>
        <th>Message</th>
    </tr>
    <tr>
        <td>{{ $new_message->name }}</td>
        <td>{{ $new_message->email }}</td>
        <td>{{ $new_message->subject }}</td>
        <td>{{ $new_message->message }}</td>
    </tr>
</table>