@extends('layout')

@section('content')
    <div class="jumbotron">
        <div class="container">
            <h1>Create a Message</h1>
        </div>
    </div>
    @if ($errors->any())
        <div class="container">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="container">
        <form method="post" action="{{ route('messages.store') }}">
            <div class="form-group">
                <label for="name">Name: </label>
                <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
                <label for="email">Email: </label>
                <input type="email" class="form-control" name="email">
            </div>
            <div class="form-group">
                <label for="subject">Subject: </label>
                <input type="text" class="form-control" name="subject">
            </div>
            <div class="form-group">
                <label for="message">Message: </label>
                <textarea class="form-control" name="message" id="message" cols="30" rows="10"></textarea>
            </div>
            {{ csrf_field() }}
            <div class="form-group">
                <input type="submit" value="Send" class="btn btn-primary">
            </div>

        </form>
    </div>

@endsection