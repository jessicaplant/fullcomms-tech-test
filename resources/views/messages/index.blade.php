@extends('layout')

@section('content')
<div class="jumbotron">
    <div class="container">
        <h1>Your Messages</h1>
        <a class="btn btn-primary" href="{{ route('messages.create') }}">Create</a>
    </div>

</div>
<div class="container">
    <table class="table">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Subject</th>
            <th>Message</th>
        </tr>
        @foreach ($messages as $message)
            <tr>
                <td>{{ $message->name }}</td>
                <td>{{ $message->email }}</td>
                <td>{{ $message->subject }}</td>
                <td>{{ $message->message }}</td>
            </tr>
        @endforeach
    </table>
</div>

@endsection